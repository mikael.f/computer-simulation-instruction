#include <stdio.h>
#include <stdlib.h>

int** alocarInstrucoes();
void desalocarInstrucoes();
void inicializarRam();
void maquina();
int somar();
int subtrair();
int multiplicar();
int dividir();
int restoDivisao();
int potencia();
int fatorial();
int velocidadeMedia();
int distancia();
void fibonacci();
int areaTriangulo();
int areaRetangulo();
int areaQuadrado();
int areaCirculo();
int areaTrapezio();
int areaLosango();
void progressaoAritmetica();
int termoDaPA();
void progressaoGeometrica();
int termoDaPG();
int volumeCubo();
int volumeParalelepipedo();
int volumeCilindro();

int main() {
    int* RAM = malloc(100 * sizeof(int));
    inicializarRam(RAM);

    printf("Lista de programas.\n");
    printf("1. Soma\n");
    printf("2. Subtracao\n");
    printf("3. Multiplicacao\n");
    printf("4. Divisao\n");
    printf("5. Resto da Divis�o\n");
    printf("6. Exponenciacao\n");
    printf("7. Velocidade Media\n");
    printf("8. Distancia\n");
    printf("9. Fatorial\n");
    printf("10. Fibonacci\n");
    printf("11. Area de um Quadrado\n");
    printf("12. Area de um Circulo (Pi = 3)\n");
    printf("13. Area de um Retangulo\n");
    printf("14. Area de um Triangulo\n");
    printf("15. Area de um Losango\n");
    printf("16. Area de um Trapezio\n");
    printf("17. Volume de um Cubo\n");
    printf("18. Volume de um Paralelepipedo\n");
    printf("19. Volume de um Cilindro (Pi = 3)\n");
    printf("20. Progressao Aritmetica (PA)\n");
    printf("21. Enesimo termo da PA\n");
    printf("22. Progress�o Geometrica (PG)\n");
    printf("23. Enesimo termo da PG\n");

    int input, n1, n2, n3;

    while (1) {
        scanf("%d", &input);
        if ((input >= 1 && input <= 8) || (input >= 13 && input <= 15) || input == 19) {
            if (input <= 6)
                printf("Insira dois termos: ");
            else if (input == 7)
                printf("Insira a distancia em km e o tempo em horas: ");
            else if (input == 8)
                printf("Insira a velocidade media em km\\h e o tempo em horas: ");
            else if (input == 13)
                printf("Insira os lados do retangulo em centimetros: ");
            else if (input == 14)
                printf("Insira a base e a altura em centimetros: ");
            else if (input == 15)
                printf("Insira as duas diagonais em centimetros: ");
            else
                printf("Insira o raio e a altura em centimetros: ");

            scanf("%d %d", &n1, &n2);
        }
        else if (input <= 12 || input == 17) {
            if (input == 9)
                printf("Insira um termo: ");
            else if (input == 10)
                printf("Insira a quantidade de termos que deseja visualizar da sequencia Fibonacci: ");
            else if (input == 11)
                printf("Insira o lado do quadrado em centimetros: ");
            else if (input == 12)
                printf("Insira o raio do circulo em centimetros: ");
            else
                printf("Insira o tamanho do lado do cubo em centimetros: ");

            scanf("%d", &n1);
        }
        else {
            if (input == 16)
                printf("Insira base maior, base menor e altura em centimetros: ");
            else if (input == 18)
                printf("Insira os 3 lados em centimetros: ");
            else if (input == 20 || input == 22)
                printf("Insira o primeiro termo, a razao e a quantidade de termos desejados: ");
            else
                printf("Insira o primeiro termo, a razao e a posicao do termo desejado: ");

            scanf("%d %d %d", &n1, &n2, &n3);
        }

        switch (input) {
        case 1:
            printf("%d + %d = %d\n", n1, n2, somar(n1, n2, RAM));
            break;

        case 2:
            printf("%d - %d = %d\n", n1, n2, subtrair(n1, n2, RAM));
            break;

        case 3:
            printf("%d x %d = %d\n", n1, n2, multiplicar(n1, n2, RAM));
            break;

        case 4:
            printf("%d / %d = %d\n", n1, n2, dividir(n1, n2, RAM));
            break;

        case 5:
            printf("%d mod %d = %d\n", n1, n2, restoDivisao(n1, n2, RAM));
            break;

        case 6:
            printf("%d ^ %d = %d\n", n1, n2, potencia(n1, n2, RAM));
            break;

        case 7:
            printf("%d km\\h\n", velocidadeMedia(n1, n2, RAM));
            break;

        case 8:
            printf("%d km\n", distancia(n1, n2, RAM));
            break;

        case 9:
            printf("%d! = %d\n", n1, fatorial(n1, RAM));
            break;

        case 10:
            fibonacci(n1, RAM);
            break;

        case 11:
            printf("%d centimetros quadrados\n", areaQuadrado(n1, RAM));
            break;

        case 12:
            printf("%d centimetros quadrados\n", areaCirculo(n1, RAM));
            break;

        case 13:
            printf("%d centimetros quadrados\n", areaRetangulo(n1, n2, RAM));
            break;

        case 14:
            printf("%d centimetros quadrados\n", areaTriangulo(n1, n2, RAM));
            break;

        case 15:
            printf("%d centimetros quadrados\n", areaLosango(n1, n2, RAM));
            break;

        case 16:
            printf("%d centimetros quadrados\n", areaTrapezio(n1, n2, n3, RAM));
            break;

        case 17:
            printf("%d centimetros cubicos\n", volumeCubo(n1, RAM));
            break;

        case 18:
            printf("%d centimetros cubicos\n", volumeParalelepipedo(n1, n2, n3, RAM));
            break;

        case 19:
            printf("%d centimetros cubicos\n", volumeCilindro(n1, n2, RAM));
            break;

        case 20:
            progressaoAritmetica(n1, n2, n3, RAM);
            break;

        case 21:
            printf("%d\n", termoDaPA(n1, n2, n3, RAM));
            break;

        case 22:
            progressaoGeometrica(n1, n2, n3, RAM);
            break;

        case 23:
            printf("%d\n", termoDaPG(n1, n2, n3, RAM));
            break;

        }
    }

    return 0;
}

int** alocarInstrucoes(int tamanho) {
    int** instrucoes = malloc(tamanho * sizeof(int*));
    for (int i = 0; i < tamanho; i++)
        instrucoes[i] = malloc(4 * sizeof(int));

    return instrucoes;
}

void desalocarInstrucoes(int** instrucoes, int tamanho) {
    for (int i = 0; i < tamanho; i++)
        free(instrucoes[i]);
    free(instrucoes);
}

void inicializarRam(int* RAM) {
    for (int i = 0; i < 100; i++)
        RAM[i] = 0;

}

void maquina(int* RAM, int** instrucoes) {
    int index = 0;
    int end1, end2, end3;
    int valor;

    int opcode = instrucoes[index][0];

    while (opcode != -1) {
        end1 = instrucoes[index][1];
        end2 = instrucoes[index][2];
        end3 = instrucoes[index][3];

        switch (opcode) {
        case 0://soma
            RAM[end3] = RAM[end1] + RAM[end2];
            break;

        case 1://subtrai
            RAM[end3] = RAM[end1] - RAM[end2];
            break;

        case 2://levar para a RAM
            RAM[end2] = instrucoes[index][1];;
            break;
        }
        index++;
        opcode = instrucoes[index][0];
    }

}

int somar(int n1, int n2, int* RAM) {
    int** instrucoes = alocarInstrucoes(4);

    instrucoes[0][0] = 2;
    instrucoes[0][1] = n1;
    instrucoes[0][2] = 0;

    instrucoes[1][0] = 2;
    instrucoes[1][1] = n2;
    instrucoes[1][2] = 1;

    instrucoes[2][0] = 0;
    instrucoes[2][1] = 0;
    instrucoes[2][2] = 1;
    instrucoes[2][3] = 2;

    instrucoes[3][0] = -1;

    maquina(RAM, instrucoes);
    int resultado = RAM[2];
    desalocarInstrucoes(instrucoes, 4);

    return resultado;
}

int subtrair(int n1, int n2, int* RAM) {
    int** instrucoes = alocarInstrucoes(4);

    instrucoes[0][0] = 2;
    instrucoes[0][1] = n1;
    instrucoes[0][2] = 0;

    instrucoes[1][0] = 2;
    instrucoes[1][1] = n2;
    instrucoes[1][2] = 1;

    instrucoes[2][0] = 1;
    instrucoes[2][1] = 0;
    instrucoes[2][2] = 1;
    instrucoes[2][3] = 2;

    instrucoes[3][0] = -1;

    maquina(RAM, instrucoes);
    int resultado = RAM[2];
    desalocarInstrucoes(instrucoes, 4);

    return resultado;
}

int multiplicar(int multiplicador, int multiplicando, int* RAM) {
    int** instrucoes = alocarInstrucoes(multiplicando + 3);

    instrucoes[0][0] = 2;
    instrucoes[0][1] = multiplicador;
    instrucoes[0][2] = 0;

    instrucoes[1][0] = 2;
    instrucoes[1][1] = 0;
    instrucoes[1][2] = 1;

    for (int i = 0; i < multiplicando; i++) {
        instrucoes[2 + i][0] = 0;
        instrucoes[2 + i][1] = 0;
        instrucoes[2 + i][2] = 1;
        instrucoes[2 + i][3] = 1;
    }

    instrucoes[multiplicando + 2][0] = -1;

    maquina(RAM, instrucoes);
    int resultado = RAM[1];
    desalocarInstrucoes(instrucoes, multiplicando + 3);

    return resultado;
}

int dividir(int dividendo, int divisor, int* RAM) {
    int** instrucoes = alocarInstrucoes(5);
    instrucoes[0][0] = 2;
    instrucoes[0][1] = dividendo;
    instrucoes[0][2] = 0;

    instrucoes[1][0] = 2;
    instrucoes[1][1] = divisor;
    instrucoes[1][2] = 1;

    instrucoes[2][0] = 2;//Contador
    instrucoes[2][1] = 0;
    instrucoes[2][2] = 2;

    instrucoes[3][0] = 2;//Valor 1 que será adicionado ao contador
    instrucoes[3][1] = 1;
    instrucoes[3][2] = 3;

    instrucoes[4][0] = -1;

    maquina(RAM, instrucoes);
    desalocarInstrucoes(instrucoes, 5);

    instrucoes = alocarInstrucoes(3);

    instrucoes[0][0] = 1;
    instrucoes[0][1] = 0;
    instrucoes[0][2] = 1;
    instrucoes[0][3] = 0;

    instrucoes[1][0] = 0;
    instrucoes[1][1] = 2;
    instrucoes[1][2] = 3;
    instrucoes[1][3] = 2;

    instrucoes[2][0] = -1;

    while (RAM[0] >= RAM[1]) {
        maquina(RAM, instrucoes);
    }

    desalocarInstrucoes(instrucoes, 2);
    int resultado = RAM[2];
    return resultado;
}

int restoDivisao(int dividendo, int divisor, int* RAM) {
    int auxiliar = dividir(dividendo, divisor, RAM);
    auxiliar = multiplicar(auxiliar, divisor, RAM);

    int** instrucoes = alocarInstrucoes(4);

    instrucoes[0][0] = 2;
    instrucoes[0][1] = dividendo;
    instrucoes[0][2] = 0;

    instrucoes[1][0] = 2;
    instrucoes[1][1] = auxiliar;
    instrucoes[1][2] = 1;

    instrucoes[2][0] = 1;
    instrucoes[2][1] = 0;
    instrucoes[2][2] = 1;
    instrucoes[2][3] = 2;

    instrucoes[3][0] = -1;

    maquina(RAM, instrucoes);
    desalocarInstrucoes(instrucoes, 4);
    int resto = RAM[2];

    return resto;
}

int potencia(int base, int expoente, int* RAM) {
    int auxiliar = base;

    for (int i = 0; i < expoente - 1; i++) {
        auxiliar = multiplicar(auxiliar, base, RAM);
    }

    return auxiliar;
}

int fatorial(int numero, int* RAM) {
    int resultado = 1;

    for (int i = numero; i > 1; i--)
        resultado = multiplicar(resultado, i, RAM);

    return resultado;
}

int velocidadeMedia(int distancia, int tempo, int* RAM) {
    return dividir(distancia, tempo, RAM);
}

int distancia(int velocidade, int tempo, int* RAM) {
    return multiplicar(velocidade, tempo, RAM);;
}

void fibonacci(int numeros, int* RAM) {
    int** instrucoes = alocarInstrucoes(numeros + 3);

    instrucoes[0][0] = 2;
    instrucoes[0][1] = 0;
    instrucoes[0][2] = 0;

    instrucoes[1][0] = 2;
    instrucoes[1][1] = 1;
    instrucoes[1][2] = 1;

    for (int i = 0; i < numeros - 2; i++) {
        instrucoes[2 + i][0] = 0;
        instrucoes[2 + i][1] = i;
        instrucoes[2 + i][2] = 1 + i;
        instrucoes[2 + i][3] = 2 + i;

    }

    instrucoes[numeros][0] = -1;

    maquina(RAM, instrucoes);
    for (int i = 0; i < numeros; i++) {
        printf("%d ", RAM[i]);

    }
    printf("\n");

    desalocarInstrucoes(instrucoes, 4);
}

int areaTriangulo(int base, int altura, int* RAM) {
    return dividir(multiplicar(base, altura, RAM),
        2, RAM);
}

int areaRetangulo(int base, int altura, int* RAM) {
    return multiplicar(base, altura, RAM);
}

int areaQuadrado(int lado, int* RAM) {
    return multiplicar(lado, lado, RAM);
}

int areaCirculo(int raio, int* RAM) {
    return multiplicar(3, multiplicar(raio, raio, RAM), RAM);
}

int areaTrapezio(int maior, int menor, int altura, int* RAM) {
    return dividir(multiplicar(maior + menor, altura, RAM),
        2, RAM);
}

int areaLosango(int maior, int menor, int* RAM) {
    return dividir(multiplicar(maior, menor, RAM),
        2, RAM);
}

void progressaoAritmetica(int primeiro, int razao, int quantidade, int* RAM) {
    int** instrucoes = alocarInstrucoes(3);

    instrucoes[0][0] = 2;
    instrucoes[0][1] = primeiro;
    instrucoes[0][2] = 0;

    instrucoes[1][0] = 2;
    instrucoes[1][1] = razao;
    instrucoes[1][2] = 1;

    instrucoes[2][0] = -1;

    maquina(RAM, instrucoes);
    desalocarInstrucoes(instrucoes, 3);

    instrucoes = alocarInstrucoes(2);

    instrucoes[0][0] = 0;
    instrucoes[0][1] = 0;
    instrucoes[0][2] = 1;
    instrucoes[0][3] = 0;

    instrucoes[1][0] = -1;

    for (int i = 0; i < quantidade; i++) {
        printf("%d ", RAM[0]);
        maquina(RAM, instrucoes);
    }

    printf("\n");

    desalocarInstrucoes(instrucoes, 2);
}

int termoDaPA(int primeiro, int razao, int quantidade, int* RAM) {
    return somar(primeiro, multiplicar(quantidade - 1, razao, RAM), RAM);
}

void progressaoGeometrica(int primeiro, int razao, int quantidade, int* RAM) {
    int auxiliar = primeiro;

    for (int i = 0; i < quantidade; i++) {
        printf("%d ", auxiliar);
        auxiliar = multiplicar(auxiliar, razao, RAM);
    }

    printf("\n");
}

int termoDaPG(int primeiro, int razao, int quantidade, int* RAM) {
    return multiplicar(primeiro, potencia(razao, quantidade - 1, RAM), RAM);
}

int volumeCubo(int lado, int* RAM) {
    return potencia(lado, 3, RAM);
}

int volumeParalelepipedo(int lado1, int lado2, int lado3, int* RAM) {
    return multiplicar(areaRetangulo(lado1, lado2, RAM), lado3, RAM);
}

int volumeCilindro(int raio, int altura, int* RAM) {
    return multiplicar(areaCirculo(raio, RAM), altura, RAM);
}
